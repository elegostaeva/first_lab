#include "header.h"


using namespace std;

void g(TritSet a) {
	a[13] = True;
	a[14] = False;
	a[15] = a[14];
	a[16] = True;
	//a[1000000000] = Unknown;
	cout << a[13] << endl << a[14] << endl << a[15] << endl << a[29] << endl << a[30] << endl << a[54] << endl;
}

void f() {
	TritSet a(27);
	g(a);
}

int main() {
	/*Trit a = True, b = False, c = Unknown;
	cout << a << endl << b << endl << c << endl << (a & b) << endl << (a | b) << endl;*/
	//f();
	TritSet a(3);
	a[0] = a[2] = True;
	a[1] = False;
	
	a[4] = a[0] & a[2];
	/*std::cout << a << std::endl;*/
	TritSet b(6);
	b[3] = True;
	b[4] = False;
	b[2] = False;
	//std::cout << b << std::endl;
	//TritSet c = a & b;
	//std::cout << c << std::endl;
	
	a[50] = Unknown;
	/*cout << a << endl;*/

	a[34] = True;
	//cout << a << endl;

	//a[34] = Unknown;
	//cout << a << endl;

	//a.shrink();
	//cout << a << endl;

	int value = a.cardinality(Unknown);
	/*cout << value << endl;*/
	std::unordered_map< Trit, size_t > x = a.cardinality();
	cout << a << endl;
	cout << b << endl;
	cout << (a | b) << endl; 
	b.trim(4);
	cout << b << endl;
	getchar();
}