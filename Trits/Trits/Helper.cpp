#include "Helper.h"

std::ostream& operator << (std::ostream& out, Helper& a) {
	out << (Trit)a;
	return out;
}

Helper& Helper::operator=(Trit a) {
	cur_tritset->set_trit(position, a);
	return *this;
}

Helper& Helper::operator=(Helper a)
{
	*this = (Trit)a;
	return *this;
}

Helper::operator Trit()
{
	return cur_tritset->get_trit(position);
}

Helper::Helper(TritSet * cur_tritset, size_t position)
{
	this->cur_tritset = cur_tritset;
	this->position = position;
}

Helper::~Helper()
{
}