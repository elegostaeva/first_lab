#pragma once
#include "Trit.h"
#include "Helper.h"
#include <unordered_map>
#include <map>
typedef unsigned int uint;
class Helper;
class TritSet
{
private:
	uint *data;
	size_t capacity;
	size_t first_capacity;
public:
	TritSet(size_t cap);
	std::unordered_map< Trit, size_t > cardinality();
	Helper& operator[](int position);
	size_t get_capacity();
	TritSet(const TritSet&); //оператор копирования
	TritSet(TritSet&&); //оператор перемещения
	void shrink();
	void trim(size_t lastIndex);
	Trit get_trit(int position);
	void set_trit(int position, Trit a);
	TritSet operator&(TritSet);
	TritSet operator|(TritSet);
	TritSet operator!();
	size_t delete_point();
	size_t length();
	size_t cardinality(Trit value);
	~TritSet();
};
std::ostream& operator<< (std::ostream&, TritSet);