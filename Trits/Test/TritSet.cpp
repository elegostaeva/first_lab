#include "pch.h"
#include "TritSet.h"

TritSet::TritSet(size_t cap)
{	
	first_capacity = cap;
	int number_of_uint = (cap * 2 + 8 * sizeof(uint) - 1) / (8 * sizeof(uint));
	capacity = number_of_uint * sizeof(uint) * 8 / 2;
	data = new uint[number_of_uint];
	for (int i = 0; i < number_of_uint; i++) {
		data[i] = 0;
	}
}

Helper& TritSet::operator[](int position)
{
	Helper *result = new Helper(this, position);
	return *result;
}

size_t TritSet::get_capacity() {
	return(capacity);
}

TritSet::TritSet(const TritSet& a) //конструктор копирования
{
	int new_number_of_uint = (a.capacity * 2 + 8 * sizeof(uint) - 1) / (8 * sizeof(uint));
	data = new uint[new_number_of_uint];
	memcpy(data, a.data, new_number_of_uint * sizeof(uint));
	this->capacity = a.capacity;
	this->first_capacity = a.first_capacity;
}

TritSet::TritSet(TritSet && other) //конструктор перемещения
{
	this->data = other.data;
	this->capacity = other.capacity;
	this->first_capacity = other.first_capacity;
	other.data = nullptr;
	other.capacity = 0;
	other.first_capacity = 0;
}

TritSet::~TritSet()
{
	delete[] data;
	//std::cout << (reinterpret_cast<size_t> (this)) << "was deleted" << std::endl;
}

size_t TritSet::delete_point()
{
	size_t last_defined_trit = length() - 1;

	if (first_capacity > last_defined_trit) {
		last_defined_trit = first_capacity;
	}
	return last_defined_trit;
}

size_t TritSet::length() {
	size_t last_defined_trit = -1;
	for (int j = 0; j < capacity; j++) {
		if ((*this)[j] != Unknown) {
			last_defined_trit = j;
		}
	}
	return (last_defined_trit + 1);
}

void TritSet::shrink() {
	size_t last_defined_trit = delete_point();
	int number_of_uint = (last_defined_trit * 2 + 8 * sizeof(uint) - 1) / (8 * sizeof(uint));
	uint *new_data = new uint[number_of_uint];
	capacity = number_of_uint * sizeof(uint) * 8 / 2;
	memcpy(new_data, data, number_of_uint * sizeof(uint));
	delete[] data;
	data = new_data;
}

void TritSet::trim(size_t lastIndex)
{
	size_t last_defined_trit = delete_point();
	for (size_t i = lastIndex; i <= delete_point(); ++i) {
		(*this)[i] = Unknown;
	}
}

Trit TritSet::get_trit(int position)
{
	if (position >= capacity) {
		return Unknown;
	}
	uint cur_uint_pos = position / sizeof(uint) / 4;
	uint cur_bit = (position - cur_uint_pos * sizeof(uint) * 4) * 2;
	uint bits_offset = sizeof(uint) * 8 - 2 - cur_bit;
	uint cur_uint = (data)[cur_uint_pos] >> bits_offset;
	uint val_bits = cur_uint & 3;
	switch (val_bits) {
	case (0):
		return Unknown;
	case (1):
		return False;
	case (2):
		return True;
	}
}

void TritSet::set_trit(int position, Trit a)
{
	if ((position >= capacity) && (a != Unknown)) {
		uint *old = data;
		int number_of_uint = (capacity * 2 + 8 * sizeof(uint) - 1) / (8 * sizeof(uint));
		int number_of_uint_for_new = (position * 2 + 8 * sizeof(uint) - 1) / (8 * sizeof(uint));
		capacity = number_of_uint_for_new * sizeof(uint) * 8 / 2;
		data = new uint[number_of_uint_for_new];
		for (int i = 0; i < number_of_uint_for_new; ++i) {
			(data)[i] = 0;
		}
		memcpy(data, old, number_of_uint * (sizeof(uint)));
		delete[] old;
	}
	if (a == this->get_trit(position)) {
		return;
	}
	uint cur_uint_pos = position / sizeof(uint) * 2 / 8;
	uint cur_bit = (position - cur_uint_pos * sizeof(uint) * 4) * 2;
	if (a == True) {
		(data)[cur_uint_pos] |= (1 << sizeof(uint) * 8 - 1 - cur_bit);
		(data)[cur_uint_pos] &= ~(1 << sizeof(uint) * 8 - 2 - cur_bit);
	}
	else if (a == Unknown) {
		(data)[cur_uint_pos] &= ~(1 << sizeof(uint) * 8 - 1 - cur_bit);
		(data)[cur_uint_pos] &= ~(1 << sizeof(uint) * 8 - 2 - cur_bit);
	}
	else {
		(data)[cur_uint_pos] &= ~(1 << sizeof(uint) * 8 - 1 - cur_bit);
		(data)[cur_uint_pos] |= (1 << sizeof(uint) * 8 - 2 - cur_bit);
	}

	return;
}

size_t TritSet::cardinality(Trit value) {
	size_t last_defined_trit = length();
	size_t result = 0;

	for (int i = 0; i < last_defined_trit; ++i) {
		if ((*this)[i] == value) {
			result++;
		}
	}
	return result;
}

TritSet TritSet::operator&(TritSet b)
{
	size_t capacity = b.get_capacity();
	if (this->get_capacity() > b.get_capacity()) {
		capacity = this->get_capacity();
	}
	TritSet c(capacity);
	for (int i = 0; i < capacity; ++i) {
		c[i] = (*this)[i] & b[i];
	}
	return c;
}

TritSet TritSet::operator|(TritSet b)
{
	size_t capacity = b.get_capacity();
	if (this->get_capacity() > b.get_capacity()) {
		capacity = this->get_capacity();
	}
	TritSet c(capacity);
	for (int i = 0; i < capacity; ++i) {
		c[i] = (*this)[i] | b[i];
	}
	return c;
}

TritSet TritSet::operator!()
{
	TritSet c(capacity);
	for (int i = 0; i < capacity; ++i) {
		c[i] = !(*this)[i];
	}
	return c;
}

std::unordered_map<Trit, size_t > TritSet::cardinality()
{
	std::unordered_map < Trit, size_t > cardinality({ {True, this->cardinality(True)}, { Unknown, this->cardinality(Unknown)}, {False, this->cardinality(False)}});
	return cardinality;
}

//std::map< Trit, int > TritSet::cardinality() {
//	std::map<Trit, int> res		({ { False, cardinality(False) },{ Unknown, cardinality(Unknown) },{ True, cardinality(True) } });
//	return res;
//}

std::ostream & operator<<(std::ostream & out, TritSet set)
{
	for (int i = 0; i < set.get_capacity(); i++) {
		out << set[i] << " ";
	}
	return out;
}
