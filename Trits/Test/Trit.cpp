#include "pch.h"
#include "Trit.h"

Trit operator&(const Trit a, const Trit b) {
	if (a == False || b == False) {
		return False;
	}
	if (a == Unknown || b == Unknown) {
		return Unknown;
	}
	return True;
}

Trit operator|(const Trit a, const Trit b) {
	if (a == True || b == True) {
		return True;
	}
	if (a == Unknown || b == Unknown) {
		return Unknown;
	}
	return False;
}

Trit operator!(const Trit a) {
	if (a == True) {
		return False;
	}
	if (a == False) {
		return True;
	}
	return Unknown;
}

std::ostream& operator<< (std::ostream& out, const Trit a) {
	switch (a) {
	case True: out << "T";
		break;
	case False: out << "F";
		break;
	case Unknown: out << "U";
		break;
	}
	return out;
}