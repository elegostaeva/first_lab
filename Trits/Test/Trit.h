#pragma once
#include <iostream>
#include <fstream>

enum Trit { True, Unknown, False};

Trit operator&(const Trit, const Trit);

Trit operator|(const Trit, const Trit);

Trit operator!(const Trit);

std::ostream& operator<< (std::ostream&, const Trit);