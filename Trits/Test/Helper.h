#pragma once
#include "Trit.h"
#include "TritSet.h"
typedef unsigned int uint;
class TritSet;
class Helper
{
private:
	size_t position;
	TritSet *cur_tritset;
public:
	Helper(TritSet *cur_tritset, size_t position);
	Helper& operator=(Trit a);
	Helper& operator=(Helper a);
	operator Trit();
	~Helper();
};
std::ostream& operator << (std::ostream& out, Helper& a);